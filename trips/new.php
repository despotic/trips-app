<?php

require_once('../lib/initialize.php');

require_login();

if (is_post_request()) {

    $target_dir = "../uploads/";
    $target_file = $target_dir . basename($_FILES["uploaded_file"]["name"]);
    $uploadOk = 1;
    $file_type = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Check if file already exists
    if (file_exists($target_file)) {
        $errors[] = "Sorry, file already exists.";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($file_type != "gpx") {
        $errors[] = "Sorry, only GPX files are allowed.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $errors[] = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["uploaded_file"]["tmp_name"], $target_file)) {
            // the file was uploaded
        } else {
            $errors[] = "Sorry, there was an error uploading your file.";
        }
    }

    $trip = [];
    $trip['trip_name'] = isset($_POST['trip_name']) ? $_POST['trip_name'] : '';

    $gpx = simplexml_load_file($target_file);

    $coordinates = [];

    foreach ($gpx->trk->trkseg as $trkpts) {
        foreach ($trkpts as $trkpt) {
            $coordinates[] = (float)$trkpt['lat'];
            $coordinates[] = (float)$trkpt['lon'];
        }
    }

    $sort_coordinates = array_chunk($coordinates, 2);

    $trip['trip_data'] = json_encode($sort_coordinates);

    unset($gpx);

    $trip['user_id'] = $_SESSION['user_id'];

    $result = insert_trip($trip);
    if ($result === true) {
        $new_id = mysqli_insert_id($db);
        $_SESSION['message'] = 'The trip was created successfully.';
        redirect_to(url_for('/trips/show.php?id=' . $new_id));
    } else {
        $errors = $result;
    }

} else {
    // display the blank form
    $trip = [];
    $trip["trip_name"] = '';
    $trip["uploaded_file"] = '';
}

?>

<?php $page_title = 'Create Trip'; ?>
<?php include(INCLUDES_PATH . '/header.php'); ?>

<div id="content">

    <a class="back-link" href="<?php echo url_for('/trips/index.php'); ?>">&laquo; Back to List</a>

    <div class="trip new">
        <h1>Create Trip</h1>

        <?php echo display_errors($errors); ?>

        <form action="<?php echo url_for('/trips/new.php'); ?>" method="post" enctype="multipart/form-data">
            <dl>
                <dt>Trip Name:</dt>
                <dd><input type="text" name="trip_name"/></dd>
            </dl>
            <dl>
                <dt>Upload GPX File of your Trip:</dt>
                <dd><input type="file" name="uploaded_file"></dd>
            </dl>
            <div id="operations">
                <input type="submit" value="Create Trip"/>
            </div>
        </form>

    </div>

</div>

<?php include(INCLUDES_PATH . '/footer.php'); ?>