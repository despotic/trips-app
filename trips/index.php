<?php require_once('../lib/initialize.php'); ?>

<?php

require_login();

$user_id = $_SESSION['user_id'];

$trip_set = find_all_trips($user_id);

?>

<?php $page_title = 'Trips'; ?>
<?php include(INCLUDES_PATH . '/header.php'); ?>

<div id="content">
    <div class="trips listing">
        <h1>Trips</h1>

        <div class="actions">
            <a class="action" href="<?php echo url_for('/trips/new.php'); ?>">Create New Trip</a>
        </div>

        <table class="list">
            <?php while ($trip = mysqli_fetch_assoc($trip_set)) { ?>
                <tr>
                    <td><?php echo h($trip['trip_name']); ?></td>
                    <td><a class="action" href="<?php echo url_for('/trips/show.php?id=' . h(u($trip['id']))); ?>">View</a></td>
                </tr>
            <?php } ?>
        </table>

        <?php
        mysqli_free_result($trip_set);
        ?>
    </div>

</div>

<?php include(INCLUDES_PATH . '/footer.php'); ?>
