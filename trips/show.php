<?php require_once('../lib/initialize.php'); ?>

<?php

require_login();

$id = isset($_GET['id']) ? $_GET['id'] : '1';

$user_id = $_SESSION['user_id'];

$trip = find_trip_by_id($id, $user_id);

?>

<?php $page_title = 'Show Trip'; ?>
<?php include(INCLUDES_PATH . '/header.php'); ?>

<div id="content">

    <a class="back-link" href="<?php echo url_for('/trips/index.php'); ?>">&laquo; Back to List</a>

    <div class="trip show">

        <h1><?php echo h($trip['trip_name']); ?></h1>

        <div class="attributes">
            <dl>
                <dt>Trip Route:</dt>
            </dl>
        </div>

        <script type="text/javascript">
            google_api_key = ''; // Your project's Google Maps API key goes here (https://code.google.com/apis/console)
            language_code = '';
            document.writeln('<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;libraries=geometry&amp;language='+(self.language_code?self.language_code:'')+'&amp;key='+(self.google_api_key?self.google_api_key:'')+'"><'+'/script>');

            thunderforest_api_key = ''; // To display OpenStreetMap tiles from ThunderForest, you need a key (http://www.thunderforest.com/docs/apikeys/)
        </script>

        <div style="margin-left:0px; margin-right:0px; margin-top:0px; margin-bottom:0px;">
            <div id="gmap_div" style="width:700px; height:576px; margin:0px; margin-right:12px; background-color:#f0f0f0; float:left; overflow:hidden;">
                <p align="center" style="font:10px Arial;">This map was created using <a target="_blank" href="http://www.gpsvisualizer.com/">GPS Visualizer</a>'s do-it-yourself geographic utilities.<br /><br />Please wait while the map data loads...</p>
            </div>

            <div id="gv_infobox" class="gv_infobox" style="font:11px Arial; border:solid #666666 1px; background-color:#ffffff; padding:4px; overflow:auto; display:none; max-width:400px;">
                <!-- Although GPS Visualizer didn't create an legend/info box with your map, you can use this space for something else if you'd like; enable it by setting gv_options.infobox_options.enabled to true -->
            </div>


            <div id="gv_tracklist" class="gv_tracklist" style="font:11px Arial; line-height:11px; background-color:#ffffff; overflow:auto; display:none;"><!-- --></div>


            <div id="gv_clear_margins" style="height:0px; clear:both;"><!-- clear the "float" --></div>
        </div>

        <br />

        <!-- begin GPS Visualizer setup script (must come after maps.google.com code) -->
        <script type="text/javascript">
            /* Global variables used by the GPS Visualizer functions (20181116115034): */
            gv_options = {};

            // basic map parameters:
            gv_options.zoom = 11;  // higher number means closer view; can also be 'auto' for automatic zoom/center based on map elements
            gv_options.map_type = 'GV_OSM';  // popular map_type choices are 'GV_STREET', 'GV_SATELLITE', 'GV_HYBRID', 'GV_TERRAIN', 'GV_OSM', 'GV_TOPO_US', 'GV_TOPO_WORLD' (http://www.gpsvisualizer.com/misc/google_map_types.html)
            gv_options.map_opacity = 1.00;  // number from 0 to 1
            gv_options.full_screen = false;  // true|false: should the map fill the entire page (or frame)?
            gv_options.width = 700;  // width of the map, in pixels
            gv_options.height = 576;  // height of the map, in pixels

            gv_options.map_div = 'gmap_div';  // the name of the HTML "div" tag containing the map itself; usually 'gmap_div'
            gv_options.doubleclick_zoom = true;  // true|false: zoom in when mouse is double-clicked?
            gv_options.doubleclick_center = true;  // true|false: re-center the map on the point that was double-clicked?
            gv_options.scroll_zoom = true; // true|false; or 'reverse' for down=in and up=out
            gv_options.page_scrolling = true; // true|false; does the map relenquish control of the scroll wheel when embedded in scrollable pages?
            gv_options.autozoom_adjustment = 0;
            gv_options.centering_options = { 'open_info_window':true, 'partial_match':true, 'center_key':'center', 'default_zoom':null } // URL-based centering (e.g., ?center=name_of_marker&zoom=14)
            gv_options.street_view = false; // true|false: allow Google Street View on the map
            gv_options.tilt = false; // true|false: allow Google to show 45-degree tilted aerial imagery?
            gv_options.animated_zoom = false; // true|false: may or may not work properly
            gv_options.disable_google_pois = false;  // true|false: if you disable clickable POIs, you also lose the labels on parks, airports, etc.

            // widgets on the map:
            gv_options.zoom_control = 'large'; // 'large'|'small'|'none'
            gv_options.recenter_button = true; // true|false: is there a 'click to recenter' option in the zoom control?
            gv_options.scale_control = true; // true|false
            gv_options.map_opacity_control = 'utilities';  // 'map'|'utilities'|'both'|false: where does the opacity control appear?
            gv_options.map_type_control = {};  // widget to change the background map
            gv_options.map_type_control.placement = 'both'; // 'map'|'utilities'|'both'|false: where does the map type control appear?
            gv_options.map_type_control.filter = false;  // true|false: when map loads, are irrelevant maps ignored?
            gv_options.map_type_control.excluded = [];  // comma-separated list of quoted map IDs that will never show in the list ('included' also works)
            gv_options.center_coordinates = true;  // true|false: show a "center coordinates" box and crosshair?
            gv_options.measurement_tools = true;  // true|false: does it appear on the map itself?
            gv_options.measurement_options = { visible:false, distance_color:'', area_color:'' };
            gv_options.crosshair_hidden = true;  // true|false: hide the crosshair initially?
            gv_options.mouse_coordinates = false;  // true|false: show a "mouse coordinates" box?
            gv_options.utilities_menu = { 'maptype':true, 'opacity':true, 'measure':true, 'export':true };
            gv_options.allow_export = false;  // true|false

            gv_options.infobox_options = {}; // options for a floating info box (id="gv_infobox"), which can contain anything
            gv_options.infobox_options.enabled = true;  // true|false: enable or disable the info box altogether
            gv_options.infobox_options.position = ['LEFT_TOP',52,6];  // [Google anchor name, relative x, relative y]
            gv_options.infobox_options.draggable = true;  // true|false: can it be moved around the screen?
            gv_options.infobox_options.collapsible = true;  // true|false: can it be collapsed by double-clicking its top bar?
            // track-related options:
            gv_options.track_tooltips = false; // true|false: should the name of a track appear on the map when you mouse over the track itself?
            gv_options.tracklist_options = {}; // options for a floating list of the tracks visible on the map
            gv_options.tracklist_options.enabled = true;  // true|false: enable or disable the tracklist altogether
            gv_options.tracklist_options.position = ['RIGHT_TOP',4,32];  // [Google anchor name, relative x, relative y]
            gv_options.tracklist_options.min_width = 100; // minimum width of the tracklist, in pixels
            gv_options.tracklist_options.max_width = 180; // maximum width of the tracklist, in pixels
            gv_options.tracklist_options.min_height = 0; // minimum height of the tracklist, in pixels; if the list is longer, scrollbars will appear
            gv_options.tracklist_options.max_height = 248; // maximum height of the tracklist, in pixels; if the list is longer, scrollbars will appear
            gv_options.tracklist_options.desc = true;  // true|false: should tracks' descriptions be shown in the list
            gv_options.tracklist_options.toggle = false;  // true|false: should clicking on a track's name turn it on or off?
            gv_options.tracklist_options.checkboxes = true;  // true|false: should there be a separate icon/checkbox for toggling visibility?
            gv_options.tracklist_options.zoom_links = true;  // true|false: should each item include a small icon that will zoom to that track?
            gv_options.tracklist_options.highlighting = true;  // true|false: should the track be highlighted when you mouse over the name in the list?
            gv_options.tracklist_options.tooltips = false;  // true|false: should the name of the track appear on the map when you mouse over the name in the list?
            gv_options.tracklist_options.draggable = true;  // true|false: can it be moved around the screen?
            gv_options.tracklist_options.collapsible = true;  // true|false: can it be collapsed by double-clicking its top bar?
            gv_options.tracklist_options.header = 'Tracks:'; // HTML code; be sure to put backslashes in front of any single quotes, and don't include any line breaks
            gv_options.tracklist_options.footer = ''; // HTML code

            // marker-related options:
            gv_options.default_marker = { color:'red',icon:'googlemini',scale:1 }; // icon can be a URL, but be sure to also include size:[w,h] and optionally anchor:[x,y]
            gv_options.vector_markers = false; // are the icons on the map in embedded SVG format?
            gv_options.marker_tooltips = true; // do the names of the markers show up when you mouse-over them?
            gv_options.marker_shadows = true; // true|false: do the standard markers have "shadows" behind them?
            gv_options.marker_link_target = '_blank'; // the name of the window or frame into which markers' URLs will load
            gv_options.info_window_width = 300;  // in pixels, the width of the markers' pop-up info "bubbles" (can be overridden by 'window_width' in individual markers)
            gv_options.thumbnail_width = 0;  // in pixels, the width of the markers' thumbnails (can be overridden by 'thumbnail_width' in individual markers)
            gv_options.photo_size = [0,0];  // in pixels, the size of the photos in info windows (can be overridden by 'photo_width' or 'photo_size' in individual markers)
            gv_options.hide_labels = false;  // true|false: hide labels when map first loads?
            gv_options.labels_behind_markers = false; // true|false: are the labels behind other markers (true) or in front of them (false)?
            gv_options.label_offset = [0,0];  // [x,y]: shift all markers' labels (positive numbers are right and down)
            gv_options.label_centered = false;  // true|false: center labels with respect to their markers?  (label_left is also a valid option.)
            gv_options.driving_directions = false;  // put a small "driving directions" form in each marker's pop-up window? (override with dd:true or dd:false in a marker's options)
            gv_options.garmin_icon_set = 'gpsmap'; // 'gpsmap' are the small 16x16 icons; change it to '24x24' for larger icons

            // Load GPS Visualizer's Google Maps functions (this must be loaded AFTER gv_options are set):
            if (window.location.toString().indexOf('https://') == 0) { // secure pages require secure scripts
                document.writeln('<script src="https://gpsvisualizer.com/google_maps/functions3.js" type="text/javascript"><'+'/script>');
            } else {
                document.writeln('<script src="http://maps.gpsvisualizer.com/google_maps/functions3.js" type="text/javascript"><'+'/script>');
            }

        </script>

        <!-- end GPSV setup script and styles; begin map-drawing script (they must be separate) -->
        <script type="text/javascript">
            function GV_Map() {

                GV_Setup_Map();

                // Track #1
                t = 1; trk[t] = {info:[],segments:[]};
                trk[t].info.name = '<?php echo h($trip['trip_name']); ?>'; trk[t].info.desc = ''; trk[t].info.clickable = true;
                trk[t].info.color = '#e60000'; trk[t].info.width = 3; trk[t].info.opacity = 0.9; trk[t].info.hidden = false; trk[t].info.z_index = null;
                trk[t].info.outline_color = 'black'; trk[t].info.outline_width = 0; trk[t].info.fill_color = '#e60000'; trk[t].info.fill_opacity = 0;
                trk[t].segments.push({ points:
                    <?php echo h($trip['trip_data']); ?> // use data from the database
                });
                GV_Draw_Track(t);

                t = 1; GV_Add_Track_to_Tracklist({bullet:'- ',name:trk[t].info.name,desc:trk[t].info.desc,color:trk[t].info.color,number:t});

                GV_Finish_Map();

            }
            GV_Map(); // execute the above code
            // http://www.gpsvisualizer.com/map_input?form=google
        </script>

    </div>

</div>

<?php include(INCLUDES_PATH . '/footer.php'); ?>