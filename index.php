<?php require_once('lib/initialize.php'); ?>

<?php require_login(); ?>

<?php $page_title = 'App Menu'; ?>
<?php include(INCLUDES_PATH . '/header.php'); ?>

<div id="content">
    <div id="main-menu">
        <h2>Main Menu</h2>
        <ul>
            <li><a href="<?php echo url_for('/trips/index.php'); ?>">View All Trips</a></li>
            <li><a href="<?php echo url_for('/trips/new.php'); ?>">Add New Trip</a></li>
        </ul>
    </div>

</div>

<?php include(INCLUDES_PATH . '/footer.php'); ?>
