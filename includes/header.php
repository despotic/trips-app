<?php
if (!isset($page_title)) {
    $page_title = 'User Area';
}
?>

<!doctype html>

<html lang="en">
<head>
    <title>Trips App - <?php echo h($page_title); ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="all" href="<?php echo url_for('/stylesheets/style.css'); ?>"/>
</head>

<body>
<header>
    <h1>User Area</h1>
</header>

<navigation>
    <ul>
        <?php if (isset($_SESSION['username'])): ?>
            <li>User: <?php echo $_SESSION['username'] ?></li>
        <?php endif; ?>
        <?php if (is_logged_in()): ?>
            <li><a href="<?php echo url_for('/index.php'); ?>">Menu</a></li>
            <li><a href="<?php echo url_for('/logout.php'); ?>">Logout</a></li>
        <?php else: ?>
            <li><a href="<?php echo url_for('/register.php'); ?>">Register</a></li>
        <?php endif; ?>
    </ul>
</navigation>

<?php echo display_session_message(); ?>