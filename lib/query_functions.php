<?php

// trips

function find_all_trips($user_id) {
    global $db;

    $sql = "SELECT * FROM TRIPS ";
    $sql .= "WHERE user_id='" . db_escape($db, $user_id) . "' ";
    //echo $sql;
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    return $result;
}

function find_trip_by_id($id, $user_id) {
    global $db;

    $sql = "SELECT * FROM TRIPS ";
    $sql .= "WHERE id='" . db_escape($db, $id) . "' ";
    $sql .= "AND user_id='" . db_escape($db, $user_id) . "' ";
    // echo $sql;
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $trip = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $trip; // returns an assoc. array
}

function validate_trip_name($trip) {
    $errors = [];

    // trip_name
    if (is_blank($trip['trip_name'])) {
        $errors[] = "Name cannot be blank.";
    } elseif (!has_length($trip['trip_name'], ['min' => 2, 'max' => 255])) {
        $errors[] = "Name must be between 2 and 255 characters.";
    }

    return $errors;
}

function insert_trip($trip) {
    global $db;

    $errors = validate_trip_name($trip);
    if (!empty($errors)) {
        return $errors;
    }

    $sql = "INSERT INTO TRIPS ";
    $sql .= "(user_id, trip_name, trip_data) ";
    $sql .= "VALUES (";
    $sql .= "'" . db_escape($db, $trip['user_id']) . "',";
    $sql .= "'" . db_escape($db, $trip['trip_name']) . "',";
    $sql .= "'" . db_escape($db, $trip['trip_data']) . "' ";
    $sql .= ")";
    $result_trips = mysqli_query($db, $sql);

    // For INSERT statements, $result is true/false
    if ($result_trips) {
        return true;
    } else {
        // INSERT failed
        echo mysqli_error($db);
        db_disconnect($db);
        exit;
    }
}

// Users

function find_user_by_username($username) {
    global $db;

    $sql = "SELECT * FROM users ";
    $sql .= "WHERE username='" . db_escape($db, $username) . "' ";
    $sql .= "LIMIT 1";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);
    return $user; // returns an assoc. array
}

function validate_user($user, $options = []) {

    $password_required = isset($options['password_required']) ? $options['password_required'] : true;

    if (is_blank($user['first_name'])) {
        $errors[] = "First name cannot be blank.";
    } elseif (!has_length($user['first_name'], array('min' => 2, 'max' => 255))) {
        $errors[] = "First name must be between 2 and 255 characters.";
    }

    if (is_blank($user['last_name'])) {
        $errors[] = "Last name cannot be blank.";
    } elseif (!has_length($user['last_name'], array('min' => 2, 'max' => 255))) {
        $errors[] = "Last name must be between 2 and 255 characters.";
    }

    if (is_blank($user['email'])) {
        $errors[] = "Email cannot be blank.";
    } elseif (!has_length($user['email'], array('max' => 255))) {
        $errors[] = "Last name must be less than 255 characters.";
    } elseif (!has_valid_email_format($user['email'])) {
        $errors[] = "Email must be a valid format.";
    }

    if (is_blank($user['username'])) {
        $errors[] = "Username cannot be blank.";
    } elseif (!has_length($user['username'], array('min' => 5, 'max' => 255))) {
        $errors[] = "Username must be between 5 and 255 characters.";
    } elseif (!has_unique_username($user['username'], isset($user['id']) ? $user['id'] : 0)) {
        $errors[] = "Username not allowed. Try another.";
    }

    if ($password_required) {
        if (is_blank($user['password'])) {
            $errors[] = "Password cannot be blank.";
        } elseif (!has_length($user['password'], array('min' => 5))) {
            $errors[] = "Password must contain 5 or more characters";
        }

        if (is_blank($user['confirm_password'])) {
            $errors[] = "Confirm password cannot be blank.";
        } elseif ($user['password'] !== $user['confirm_password']) {
            $errors[] = "Password and confirm password must match.";
        }
    }

    return $errors;
}

function insert_user($user) {
    global $db;

    $errors = validate_user($user);
    if (!empty($errors)) {
        return $errors;
    }

    $hashed_password = password_hash($user['password'], PASSWORD_BCRYPT);

    $sql = "INSERT INTO users ";
    $sql .= "(first_name, last_name, email, username, hashed_password) ";
    $sql .= "VALUES (";
    $sql .= "'" . db_escape($db, $user['first_name']) . "',";
    $sql .= "'" . db_escape($db, $user['last_name']) . "',";
    $sql .= "'" . db_escape($db, $user['email']) . "',";
    $sql .= "'" . db_escape($db, $user['username']) . "',";
    $sql .= "'" . db_escape($db, $hashed_password) . "'";
    $sql .= ")";
    $result = mysqli_query($db, $sql);

    // For INSERT statements, $result is true/false
    if ($result) {
        return true;
    } else {
        // INSERT failed
        echo mysqli_error($db);
        db_disconnect($db);
        exit;
    }
}