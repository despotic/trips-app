<?php
ob_start(); // output buffering is turned on

session_start(); // turn on sessions

define("LIB_PATH", dirname(__FILE__));
define("PROJECT_PATH", dirname(LIB_PATH));
define("INCLUDES_PATH", PROJECT_PATH . '/includes');

$script_end = strpos($_SERVER['SCRIPT_NAME'], '/trips-app') + 10;
$doc_root = substr($_SERVER['SCRIPT_NAME'], 0, $script_end);
define("WWW_ROOT", $doc_root);

require_once('functions.php');
require_once('database.php');
require_once('query_functions.php');
require_once('validation_functions.php');
require_once('auth_functions.php');

$db = db_connect();
create_users_table($db);
create_trips_table($db);
$errors = [];