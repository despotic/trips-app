<?php

require_once('db_credentials.php');

function db_connect() {
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    confirm_db_connect();
    return $connection;
}

function db_disconnect($connection) {
    if (isset($connection)) {
        mysqli_close($connection);
    }
}

function db_escape($connection, $string) {
    return mysqli_real_escape_string($connection, $string);
}

function confirm_db_connect() {
    if (mysqli_connect_errno()) {
        $msg = "Database connection failed: ";
        $msg .= mysqli_connect_error();
        $msg .= " (" . mysqli_connect_errno() . ")";
        exit($msg);
    }
}

function confirm_result_set($result_set) {
    if (!$result_set) {
        exit("Database query failed.");
    }
}

function create_users_table($db) {
    $sql = "CREATE TABLE IF NOT EXISTS USERS (
            id INT(6) AUTO_INCREMENT PRIMARY KEY, 
            first_name VARCHAR(255),
            last_name VARCHAR(255),
            email VARCHAR(255),
            username VARCHAR(255),
            hashed_password VARCHAR(255)
          )";

    if (!mysqli_query($db, $sql)) {
        $msg = "Error creating table: ";
        $msg .= mysqli_error($db);
        exit($msg);
    }
}

function create_trips_table($db) {
    $sql = "CREATE TABLE IF NOT EXISTS TRIPS (
            id INT(6) AUTO_INCREMENT PRIMARY KEY,
            user_id INT(6),
            trip_name VARCHAR(255),
            trip_data BLOB,
            FOREIGN KEY (user_id) REFERENCES USERS(id)
          )";

    if (!mysqli_query($db, $sql)) {
        $msg = "Error creating table: ";
        $msg .= mysqli_error($db);
        exit($msg);
    }
}