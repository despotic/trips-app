<?php

// Performs all actions necessary to log in an user
function log_in_user($user) {
    // Renerating the ID protects the user from session fixation.
    session_regenerate_id();
    $_SESSION['user_id'] = $user['id'];
    $_SESSION['last_login'] = time();
    $_SESSION['username'] = $user['username'];
    return true;
}

// Performs all actions necessary to log out an user
function log_out_user() {
    unset($_SESSION['user_id']);
    unset($_SESSION['last_login']);
    unset($_SESSION['username']);
    return true;
}

// is_logged_in() contains all the logic for determining if a
// request should be considered a "logged in" request or not.
function is_logged_in() {
    return isset($_SESSION['user_id']);
}

// Call require_login() at the top of any page which needs to
// require a valid login before granting acccess to the page.
function require_login() {
    if (!is_logged_in()) {
        redirect_to(url_for('/login.php'));
    } else {
        // Do nothing, let the rest of the page proceed
    }
}