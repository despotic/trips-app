<?php
require_once('lib/initialize.php');

log_out_user();
redirect_to(url_for('/login.php'));