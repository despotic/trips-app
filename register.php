<?php

require_once('lib/initialize.php');

if (is_post_request()) {
    $user = [];
    $user['first_name'] = isset($_POST['first_name']) ? $_POST['first_name'] : '';
    $user['last_name'] = isset($_POST['last_name']) ? $_POST['last_name'] : '';
    $user['email'] = isset($_POST['email']) ? $_POST['email'] : '';
    $user['username'] = isset($_POST['username']) ? $_POST['username'] : '';
    $user['password'] = isset($_POST['password']) ? $_POST['password'] : '';
    $user['confirm_password'] = isset($_POST['confirm_password']) ? $_POST['confirm_password'] : '';

    $result = insert_user($user);
    if ($result === true) {
        $new_id = mysqli_insert_id($db);
        $_SESSION['message'] = 'User created successfully. Please Log In.';
        redirect_to(url_for('/login.php'));
    } else {
        $errors = $result;
    }

} else {
    // display the blank form
    $user = [];
    $user["first_name"] = '';
    $user["last_name"] = '';
    $user["email"] = '';
    $user["username"] = '';
    $user['password'] = '';
    $user['confirm_password'] = '';
}

?>

<?php $page_title = 'Create User'; ?>
<?php include(INCLUDES_PATH . '/header.php'); ?>

<div id="content">

    <a class="back-link" href="<?php echo url_for('/login.php'); ?>">&laquo; Back to Login</a>

    <div class="user new">
        <h1>Create User</h1>

        <?php echo display_errors($errors); ?>

        <form action="<?php echo url_for('/register.php'); ?>" method="post">
            <dl>
                <dt>First name</dt>
                <dd><input type="text" name="first_name" value="<?php echo h($user['first_name']); ?>"/></dd>
            </dl>

            <dl>
                <dt>Last name</dt>
                <dd><input type="text" name="last_name" value="<?php echo h($user['last_name']); ?>"/></dd>
            </dl>

            <dl>
                <dt>Username</dt>
                <dd><input type="text" name="username" value="<?php echo h($user['username']); ?>"/></dd>
            </dl>

            <dl>
                <dt>Email</dt>
                <dd><input type="text" name="email" value="<?php echo h($user['email']); ?>"/><br/></dd>
            </dl>

            <dl>
                <dt>Password</dt>
                <dd><input type="password" name="password" value=""/></dd>
            </dl>

            <dl>
                <dt>Confirm Password</dt>
                <dd><input type="password" name="confirm_password" value=""/></dd>
            </dl>
            <br/>

            <div id="operations">
                <input type="submit" value="Create User"/>
            </div>
        </form>

    </div>

</div>

<?php include(INCLUDES_PATH . '/footer.php'); ?>