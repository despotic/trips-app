<?php
require_once('lib/initialize.php');

$errors = [];
$username = '';
$password = '';

if (is_post_request()) {

    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';

    // Validations
    if (is_blank($username)) {
        $errors[] = "Username cannot be blank.";
    }
    if (is_blank($password)) {
        $errors[] = "Password cannot be blank.";
    }

    // if there were no errors, try to login
    if (empty($errors)) {
        // Using one variable ensures that msg is the same
        $login_failure_msg = "Log in was unsuccessful.";

        $user = find_user_by_username($username);
        if ($user) {

            if (password_verify($password, $user['hashed_password'])) {
                // password matches
                log_in_user($user);
                redirect_to(url_for('/index.php'));
            } else {
                // username found, but password does not match
                $errors[] = $login_failure_msg;
            }

        } else {
            // no username found
            $errors[] = $login_failure_msg;
        }

    }
}

?>

<?php $page_title = 'Log in'; ?>
<?php include(INCLUDES_PATH . '/header.php'); ?>

<div id="content">
    <h1>Log in</h1>
    <p>In order to proceed please log in bellow.</p>

    <?php echo display_errors($errors); ?>

    <form action="login.php" method="post">
        Username:<br/>
        <input type="text" name="username" value="<?php echo h($username); ?>"/><br/>
        Password:<br/>
        <input type="password" name="password" value=""/><br/>
        <input type="submit" name="submit" value="Submit"/>
    </form>

    <p>Don't have an account? <a href="<?php echo url_for('/register.php'); ?>">Register here.</a></p>

</div>

<?php include(INCLUDES_PATH . '/footer.php'); ?>